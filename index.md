---
layout: default
title: Fanwork.moe - Hub of Japanese media fanwork!
description: List of known fanwork that is related to Japanese idol or music such as translations of lyrics, posts, books, videos, etc. Also links to official translations if available.
---

All websites we're awared of who make translations of Japanese media are listed down here by show or artist or group names.

Quick links:
- [Anime (Japan)](#anime-japan)
- [Anime (Official Subtitles)](#anime-official-subtitles)
- [Anime (Fansubs)](#anime-fansubs)
- [Manga (Japan)](#manga-japan)
- [Manga (Official Translations)](#manga-official-translations)
- [Manga (Fanlations)](#manga-fanlations)
- [Vocaloid and Utau](#vocaloid-and-utau)
- [Idols](#idols)
- [Gaki no Tsukai](#gaki-no-tsukai)
- [Others](#others)


## Anime (Japan)
NOTICE: Most of them are region locked to Japan.
- [![AbemaTV](/img/anime/abematv.png "AbemaTV") <span>AbemaTV<br />&nbsp;</span>](https://abema.tv/)
{: .undo-ul}
{: .portfollio-icons}


## Anime (Official Subtitles)
NOTICE: Service availability depends on your location.

- [![Crunchyroll](/img/anime/crunchyroll.png "Crunchyroll") <span>Crunchyroll<br />&nbsp;</span>](https://www.crunchyroll.com/videos/anime)
- [![FUNimation](/img/anime/funimation.png "FUNimation") <span>FUNimation<br />&nbsp;</span>](https://www.funimation.com/videos/simulcasts_shows)
- [![AnimeLab](/img/anime/animelab.png "AnimeLab") <span>MADMAN's<br />AnimeLab</span>](https://www.animelab.com/)
- [![Netflix](/img/anime/netflix.png "Netflix") <span>Netflix<br />&nbsp;</span>](https://www.netflix.com)
- [![iflix](/img/anime/iflix.png "iflix") <span>iflix<br />&nbsp;</span>](https://piay.iflix.com/tv/genre/anime)
- [![Viu HK](/img/anime/viu-hk.png "Viu HK") <span>Viu<br />HK</span>](https://www.viu.com/)
- [![Ani Gamer](/img/anime/ani-gamer.png "Ani Gamer") <span lang="zh-Hant">Ani Gamer<br />動畫瘋</span>](https://ani.gamer.com.tw/index.php)
- [![LiTV](/img/anime/litv.png "LiTV") <span lang="zh-Hant">LiTV<br />立視線上影視</span>](https://www.litv.tv/vod/comic)
- [![iQIYI](/img/anime/iqiyi.png "iQIYI") <span lang="zh-Hans">iQIYI<br />爱奇艺</span>](https://www.iqiyi.com)
- [![QQ Video](/img/anime/qqvideo.png "QQ Video") <span lang="zh-Hans">QQ Video<br />腾讯视频</span>](https://v.qq.com/cartoon/)
- [![bilibili](/img/anime/bilibili.png "bilibili") <span lang="zh-Hans">bilibili<br />哔哩哔哩</span>](https://bangumi.bilibili.com)
- [![AniplusTV](/img/anime/aniplustv.png "AniplusTV") <span lang="kr">AniplusTV<br />애니플러스</span>](https://www.aniplustv.com/#/tv/)
- [![Aniplus Asia](/img/anime/aniplustv.png "Aniplus Asia") <span>Aniplus<br />Asia</span>](https://www.aniplus-asia.com/)
- [![Wakanim](/img/anime/wakanim.png "Wakanim") <span>Wakanim<br />&nbsp;</span>](https://www.wakanim.tv/)
- [![Flixer](/img/anime/flixer.png "Flixer") <span>Flixer<br />&nbsp;</span>](https://www.flixerapp.com/)
- [![Astro Go](/img/anime/astrogo.png "Astro Go") <span>Astro<br />Go</span>](https://my.onthego.astro.com.my/)
- [![anime on demand de](/img/anime/animeondemand-de.png "anime-on-demand.de") <span>anime-on-<br />demand.de</span>](https://anime-on-demand.de/)
- [![Yamato Animation](/img/anime/yamatoanimation.png "Yamato Animation") <span>Yamato<br />Animation</span>](https://www.youtube.com/user/YamatoEntertainment/)
{: .undo-ul}
{: .portfollio-icons}


## Anime (Fansubs)
- [![FFFansubs](/img/anime/fffansubs.png "FFFansubs") <span>FFFansubs<br />&nbsp;</span>](https://fffansubs.org/)
- [![Doki Fansubs](/img/anime/doki-fansubs.png "Doki Fansubs") <span>Doki<br />Fansubs</span>](https://doki.co)
- [![Erai-raws](/img/anime/erai-raws.png "Erai-raws") <span>Erai-raws<br />&nbsp;</span>](https://erai-raws.info)
{: .undo-ul}
{: .portfollio-icons}


## Manga (Official)
NOTICE: Service availability depends on your location.
- [![Crunchyroll](/img/anime/crunchyroll.png "Crunchyroll") <span>Crunchyroll<br />&nbsp;</span>](https://www.crunchyroll.com/comics/manga)
{: .undo-ul}
{: .portfollio-icons}


## Manga (Scanlations)
- [![WhiteLies Fansub](/img/manga/whitelies.png "WhiteLies Fansub") <span>WhiteLies Fansub<br />(Spanish)</span>](https://whiteliesfansub.com/)
- [![nijiYOAKE](/img/manga/nijiyoake.png "nijiYOAKE") <span>nijiYOAKE<br />&nbsp;</span>](https://nijiyoake.tumblr.com/)
- [![Akagamins](/img/manga/akagamins.png "Akagamins") <span>Akagamins<br />&nbsp;</span>](https://akagamins.tumblr.com/)
{: .undo-ul}
{: .portfollio-icons}


## Idols
### Multiple groups
- [![Conjyak](/img/idols/conjyak.png "Conjyak") <span>Conjyak<br />&nbsp;</span>](https://conjyak.com/)
{: .undo-ul}
{: .portfollio-icons}


### AKB48 and sister/rival groups
- [![Stage48](/img/idols/stage48.png "Stage48") <span>Stage48<br />&nbsp;</span>](http://stage48.net/forum/index.php)
- [![Studio48](/img/idols/studio48.png "Studio48") <span>Studio48<br />&nbsp;</span>](http://stage48.net/studio48/index.html)
- [![Wiki48](/img/idols/wiki48.png "Wiki48") <span>Wiki48<br />&nbsp;</span>](http://stage48.net/wiki/index.php/Main_Page)
- [![Translate48](/img/idols/translate48.png "Translate48") <span>Translate48<br />&nbsp;</span>](https://translate48.blogspot.com/)
{: .undo-ul}
{: .portfollio-icons}


### LoveLive! franchise
- [![Onibe](/img/idols/onibe.png "Onibe") <span>Onibe<br />&nbsp;</span>](https://onibe.moe/)
{: .undo-ul}
{: .portfollio-icons}


## Vocaloid and Utau
- will be updated.


## Gaki no Tsukai
- [![Reddit](/img/gaki/reddit-gnt.png "r/GakiNoTsukai") <span>GakiNoTsukai<br />Subreddit</span>](https://www.reddit.com/r/GakiNoTsukai)
- [![Gaki no Tsukai Discord](/img/gaki/gaki-discord.png "Gaki no Tsukai Discord") <span>Gaki no Tsukai<br />Discord</span>](https://discord.gg/kQHx3Ha)
- [![Forum](/img/gaki/forum-gnt.png "Gaki No Tsukai Fan Page / Forum") <span>Gaki No Tsukai<br />Fan Page / Forum</span>](https://gaki-no-tsukai.com/)
- [![Team Gaki](/img/gaki/team-gaki.png "Team Gaki") <span>Subbing<br />Team Gaki</span>](https://www.teamgaki.com/)
- [![Watch GNT](/img/gaki/watch-gnt.png "watchgakinotsukai.blogspot.com") <span>watchgakinotsukai<br />blogspot.com</span>](https://watchgakinotsukai.blogspot.com/)
- [![Gaki Archives](/img/gaki/gaki-archives.png "Gaki Archives") <span>Gaki<br />Archives</span>](https://www.gakiarchives.com/)
- [![DTR Subs](/img/gaki/dtr-subs.png "DTR Subs") <span>DTR<br />Subs</span>](https://dtr-subs.tumblr.com/)
- [![Tofu Panda Fansubs](/img/gaki/tpf-subs.png "Tofu Panda Fansubs") <span>Tofu Panda<br />Fansubs</span>](https://tofupandas.tumblr.com/)
- [![GakiFiles](/img/gaki/gaki-files.png "GakiFiles") <span>GakiFiles<br />Subs</span>](https://gakifiles.blogspot.com/)
- [![Shion no Tsukai](/img/gaki/shionnotsukai.png "Shion no Tsukai") <span>Shion<br />no Tsukai</span>](https://shionnotsukai.wordpress.com)
- [![Gaki Wiki](/img/gaki/gaki-wiki.png "Gaki Wiki") <span>Gaki<br />Wiki</span>](https://gakinotsukai.wikia.com/)
- [![Conjyak](/img/gaki/conjyak.png "Conjyak") <span>Conjyak<br />&nbsp;</span>](https://conjyak.com/category/gaki-no-tsukai/)
- [![Gaki Tracker on Braxnet](/img/gaki/gaki-tracker.png "Gaki Tracker on Braxnet") <span>Gaki Tracker<br />on Braxnet</span>](https://braxnet.org/gaki/)
- [![VK Gaki Group](/img/gaki/vk-gaki-group.png "VK Gaki Group") <span>VK Gaki<br />Group</span>](https://vk.com/videos-7016284)
- [![OolongCha Translations](/img/gaki/oolongcha.png "OolongCha Translations") <span>OolongCha<br />Translations</span>](https://oolongcha.net/)
- [![DuckToaster Livestream](/img/gaki/ducktoaster.png "DuckToaster Livestream") <span>DuckToaster<br />Livestream</span>](https://vaughnlive.tv/ducktoaster)
- [![J-Hive](/img/gaki/j-hive.png "J-Hive") <span>J-Hive<br />&nbsp;</span>](https://www.facebook.com/jhivesubs/)
- [![HouseiGIFs](/img/gaki/houseigifs.png "HouseiGIFs") <span>HouseiGIFs<br />&nbsp;</span>](https://houseigifs.tumblr.com/)
{: .undo-ul}
{: .portfollio-icons}


## Others
Other fanwork will be added here soon.


