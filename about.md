---
layout: page
title: About
permalink: /about/
---

Fanwork.moe was created by MNH48 Proxy Group (MPG) to gather all links of
Japanese media-related fanwork in one place to make it easy to find. The domain
itself is being provided by our sister, muhdnurhidayat of MNH48.moe website.

We, MPG also have our own website at mpg.fanwork.moe where we pay muhdnurhidayat
to do translations for us. As she is MNH48, we name ourselves as MNH48 Proxy
Group as we're releasing stuffs that she has translated. However, please note
that she don't do anything other than translation. She don't even see the raw
images or videos, we only gave the text itself to her for translate. She usually
only do legal translation works but made exception for us as we're best friends.
As such, if there's anything wrong with our releases, do NOT contact her, but
contact us.

Please be aware that muhdnurhidayat is also not directly related to the running
of this website (Fanwork.moe), so please avoid bothering her if there's anything
happening on this website. Please just [open an issue](https://gitlab.com/fanwork/fanwork.gitlab.io/issues)
or [send us email](mailto:admin@fanwork.moe) if there's anything important, and
please also include proof of ownership if you want to take down something from
our website.

This website is running the new Jekyll theme which you can find out more at:
{% include icon-github.html username="jglovier" %} /
[jekyll-new](https://github.com/jglovier/jekyll-new) and you can find the source
code for Jekyll at {% include icon-github.html username="jekyll" %} /
[jekyll](https://github.com/jekyll/jekyll)
